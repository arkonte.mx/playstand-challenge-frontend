# PayStand Challenge: Frontend
## Setup
Before setting up the app, make sure you already have [Docker](https://docs.docker.com/get-docker/) installed on your system.

After cloning the repository, in order to run the frontend you must execute the following command inside the root folder of the proyect:
### For Windows
```bash
> setup_on_windows.bat
```
### For Linux
```sh
$ sh setup_on_linux.sh
```

##### **Once the setup is complete the [app](http://localhost:3000) will run on port 3000**

## Backend Application
Before using this application, pleas setup the [PayStand Challenge Backend](https://gitlab.com/arkonte.mx/playstand-challenge-backend.git)

## Errata
### Windows
If the setup fails, please do the following:
- Open Docker Desktop dashboard
- Delete the project's container
- Go to Settings
  - Click on "Resources"
    - Click on "FILE SHARING"
      - Add the project's folder to the list of directories
- Run the setup again