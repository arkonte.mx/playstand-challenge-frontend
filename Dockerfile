FROM node:latest
EXPOSE 3000
WORKDIR /usr/src/app
COPY package*.json ./
RUN yarn install
COPY . .
CMD [ "yarn", "start" ]