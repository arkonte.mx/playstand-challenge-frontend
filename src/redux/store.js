import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import userReducer, { restoreSessionAction } from './userDuck';
import taskReducer, { restoreTaskAction } from './taskDuck';

import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    user: userReducer,
    task: taskReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const generateStore = () => {

    const store = createStore(
        rootReducer,
        composeEnhancers( applyMiddleware(thunk) )
    );

    restoreSessionAction()(store.dispatch);
    restoreTaskAction()(store.dispatch, store.getState);

    return store;
}

export default generateStore;