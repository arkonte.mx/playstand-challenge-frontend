import axios from 'axios';
import { EMPTY_STRING, ZERO, ONE } from '../constants/common';
import { EMPTY_TASK_ID, EMPTY_TASK_TITLE, USER_NOT_LOGGED_IN } from '../constants/error_messages';
import { isLoggedIn } from '../utils/session';

const INITIAL_DATA = {
  is_processing: false,
  tasks: []
}

const FETCH_TASKS = "FETCH_TASKS";
const FETCH_TASKS_SUCCESS = "FETCH_TASKS_SUCCESS";
const FETCH_TASKS_ERROR = "FETCH_TASKS_ERROR";

const CREATE_TASK = "CREATE_TASK";
const CREATE_TASK_SUCCESS = "CREATE_TASK_SUCCESS";
const CREATE_TASK_ERROR = "CREATE_TASK_ERROR";

const UPDATE_TASK = "UPDATE_TASK";
const UPDATE_TASK_SUCCESS = "UPDATE_TASK_SUCCESS";
const UPDATE_TASK_ERROR = "UPDATE_TASK_ERROR";

const COMPLETE_TASK = "COMPLETE_TASK";
const COMPLETE_TASK_SUCCESS = "COMPLETE_TASK_SUCCESS";
const COMPLETE_TASK_ERROR = "COMPLETE_TASK_ERROR";

const DELETE_TASK = "DELETE_TASK";
const DELETE_TASK_SUCCESS = "DELETE_TASK_SUCCESS";
const DELETE_TASK_ERROR = "DELETE_TASK_ERROR";

const RESTORE_TASKS = "RESTORE_TASKS";
const RESTORE_TASKS_SUCCESS = "RESTORE_TASKS_SUCCESS";
const RESTORE_TASKS_ERROR = "RESTORE_TASKS_ERROR";

const API_URL = process.env.REACT_APP_API_URL;

// REDUCER
export default function reducer(state = INITIAL_DATA, { type = EMPTY_STRING, payload = {} }) {
  const { message = EMPTY_STRING, tasks = [] } = payload;

  switch(type) {
    case FETCH_TASKS:
      return { ...state, is_processing: true };
    case FETCH_TASKS_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case FETCH_TASKS_ERROR:
      return { ...state, is_processing: false, message }
    case CREATE_TASK:
      return { ...state, is_processing: true }
    case CREATE_TASK_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case CREATE_TASK_ERROR:
      return { ...state, is_processing: false, message };
    case COMPLETE_TASK:
      return { ...state, is_processing: true };
    case COMPLETE_TASK_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case COMPLETE_TASK_ERROR:
      return { ...state, is_processing: false, message };
    case UPDATE_TASK:
      return { ...state, is_processing: true };
    case UPDATE_TASK_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case UPDATE_TASK_ERROR:
      return { ...state, is_processing: false, message };
    case DELETE_TASK:
      return { ...state, is_processing: true }
    case DELETE_TASK_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case DELETE_TASK_ERROR:
      return { ...state, is_processing: false, message };
    case RESTORE_TASKS:
      return { ...state, is_processing: true };
    case RESTORE_TASKS_SUCCESS:
      return { ...state, is_processing: false, tasks };
    case RESTORE_TASKS_ERROR:
      return { ...state, is_processing: false, message }
    default:
      return state
  }
}

// THUNK
export const restoreTaskAction = () => (dispatch, getState) => {
  fetchTasksAction()(dispatch, getState);
}

export const fetchTasksAction = () => async (dispatch, getState) => {
  dispatch({ type: FETCH_TASKS });

  const current_state = getState();
  const { user = {} } = current_state;

  try {

    checkUpRequest(current_state);

    const { data: response = [] } = await axios.get(API_URL, { params: { uid: user.uid } });

    dispatch({
      type: FETCH_TASKS_SUCCESS,
      payload: { tasks: response }
    });

  } catch (message) {
    dispatch({
      type: FETCH_TASKS_ERROR,
      payload:  { message }
    });
  }
}

export const createTaskAction = (task_to_save = {}) => async (dispatch, getState) => {
  dispatch({ type: CREATE_TASK });

  const current_state = getState();
  const { user = {} } = current_state;

  try {

    checkUpRequest(current_state, task_to_save);

    const { task: taskStore = {} } = getState();
    const { data: response = {} } = await axios.post(API_URL, { ...task_to_save, uid: user.uid });

    const tasks = [ ...(taskStore.tasks || []), response ];

    dispatch({
      type: CREATE_TASK_SUCCESS,
      payload: { tasks }
    });

  } catch (message) {
    dispatch({
      type: CREATE_TASK_ERROR,
      payload:  { message }
    });
  }
}

export const deleteTaskAction = (id = ZERO) => async (dispatch, getState) => {
  dispatch({ type: DELETE_TASK });

  const current_state = getState();
  const { user = {} } = current_state;

  try {

    checkUpRequest(current_state, { id });

    const { task: taskStore = {} } = getState();
    const { tasks = [] } = taskStore;

    const { data: response = {} } = await axios.delete(`${API_URL}/:id`, { data: { id, uid: user.uid } });

    dispatch({
      type: DELETE_TASK_SUCCESS,
      payload: {
        tasks: tasks.filter((task) =>
          task.id !== response.id)
      }
    });

  } catch (message) {
    dispatch({
      type: DELETE_TASK_ERROR,
      payload:  { message }
    });
  }
}

export const completeTaskAction = (id = ZERO) => async (dispatch, getState) => {
  dispatch({ type: COMPLETE_TASK });

  const current_state = getState();
  const { user = {},  } = current_state;

  try {

    checkUpRequest(current_state, { id });

    const { task: taskStore = {} } = getState();
    let tasks = (taskStore.tasks || []);

    const { data: response = {} } = await axios.patch(`${API_URL}/:id`, { id, uid: user.uid });

    const completed_index = tasks
      .findIndex(task => task.id === response.id);

    tasks[completed_index] = response;

    dispatch({
      type: COMPLETE_TASK_SUCCESS,
      payload: { tasks: [ ...tasks ] }
    });

  } catch (message) {
    dispatch({
      type: COMPLETE_TASK_ERROR,
      payload:  { message }
    });
  }
}

export const updateTaskAction = (update = {}) => async (dispatch, getState) => {
  dispatch({ type: UPDATE_TASK });

  const current_state = getState();
  const { user = {},  } = current_state;

  try {

    checkUpRequest(current_state, { id: update.id });

    const { task: taskStore = {} } = getState();
    let tasks = (taskStore.tasks || []);

    const { data: response = {} } = await axios.put(`${API_URL}/:id`, { ...update, uid: user.uid });

    const updated_index = tasks
      .findIndex(task => task.id === response.id);

    tasks[updated_index] = response;

    dispatch({
      type: UPDATE_TASK_SUCCESS,
      payload: { tasks: [ ...tasks ] }
    });

  } catch (message) {
    dispatch({
      type: UPDATE_TASK_ERROR,
      payload:  { message }
    });
  }
}

const checkUpRequest = (current_state = {}, data_to_validate = {}) => {
  const { user = {} } = current_state;

  const validations = {
    login: () => {
      return !isLoggedIn(user)
        && USER_NOT_LOGGED_IN;
    },
    id: () => {
      const { id } = data_to_validate;

      return (typeof id === "number" && id < ONE)
        && EMPTY_TASK_ID;
    },
    title: () => {
      const { title } = data_to_validate;

      return (typeof title === "string" && title.trim() === EMPTY_STRING)
        && EMPTY_TASK_TITLE;
    }
  }

  let error = null;

  for (const validation_name of Object.keys(validations)) {
    const validation = validations[validation_name] || (() => {});

    error = validation();

    if(error) {
      break;
    }
  }

  if(error) {
    throw error;
  }
}