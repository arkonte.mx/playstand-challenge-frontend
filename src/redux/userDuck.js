import { EMPTY_STRING } from '../constants/common';
import { STORED_USER } from '../constants/storage';
import { loginWithGoogle, logLogin, logoutFromGoogle } from '../firebase';
import { addToLocalStorage, clearLocalStorage, getFromLocalStorage } from '../utils/local_storage';
import { fetchTasksAction } from './taskDuck';

const INITIAL_DATA = {
  is_logged_in: false,
  is_fetching_data: false
}

const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGIN_ERROR = "LOGIN_ERROR";

const LOGOUT = "LOGOUT";
const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
const LOGOUT_ERROR = "LOGOUT_ERROR";

// REDUCER
export default function reducer(state = INITIAL_DATA, { type = EMPTY_STRING, payload = {} }) {
  const { message = EMPTY_STRING } = payload;

  switch(type) {
    case LOGIN:
      return { ...state, is_fetching_data: true, is_logged_in: false };
    case LOGIN_SUCCESS:
      return { ...state, is_fetching_data: false, is_logged_in: true, ...payload }
    case LOGIN_ERROR:
      return { ...state, is_fetching_data: false, is_logged_in: false, message }
    case LOGOUT:
      return { ...state, is_fetching_data: true };
    case LOGOUT_SUCCESS:
      return { ...INITIAL_DATA }
    case LOGOUT_ERROR:
      return { ...state, is_fetching_data: false, message }
    default:
      return state
  }
}

// THUNK
export const restoreSessionAction = () => (dispatch) => {
    const user = getFromLocalStorage(STORED_USER);

    if (user !== undefined) {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: user
        })
    }
}

export const loginWithGoogleAction = () => async (dispatch, getState) => {

    dispatch({ type: LOGIN });

    try {

      const { user = {} } = await loginWithGoogle();
      const { uid = EMPTY_STRING, displayName = EMPTY_STRING, email = EMPTY_STRING, refreshToken = EMPTY_STRING, photoURL = EMPTY_STRING } = user;

      const payload = {
        uid,
        displayName,
        email,
        refreshToken,
        photoURL: photoURL.replace(/ /g, EMPTY_STRING)
      };

      dispatch({ type: LOGIN_SUCCESS, payload });

      fetchTasksAction()(dispatch, getState);
      logLogin(payload);

    } catch (ex) {

      dispatch({
        type: LOGIN_ERROR,
        payload: { message: ex.message }
      });

    } finally {
      addToLocalStorage(STORED_USER, getState().user);
    }
}

export const logoutFromGoogleAction = () => async (dispatch, getState) => {

  dispatch({ type: LOGOUT });

  try {

    logoutFromGoogle();
    dispatch({ type: LOGOUT_SUCCESS });
    clearLocalStorage();

  } catch (ex) {

    dispatch({
      type: LOGOUT_ERROR,
      payload: ex.message
    });

  } finally {
    addToLocalStorage(STORED_USER, getState().user);
  }
}

