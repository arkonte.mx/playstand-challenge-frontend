export const GENERIC_TASK_INSERT_ERROR_MESSAGE = "Some error occurred while creating the task";
export const GENERIC_TASK_RETRIEVE_ERROR_MESSAGE = "Some error occurred while retreaving tasks";
export const GENERIC_TASK_UPDATE_ERROR_MESSAGE = "Some error occurred while updating tasks";
export const GENERIC_TASK_DELETE_ERROR_MESSAGE = "Some error occurred while deleting tasks";

export const GENERIC_ERROR_MESSAGE = "Oops, something went wrong!";
