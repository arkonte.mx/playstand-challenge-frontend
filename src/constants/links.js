import { SLASH, COLON } from '../constants/common';

export const TODO = "Todo";
export const USER = "User";
export const LOGIN = "Login";
export const ID = "Id";

export const ROOT_LINK = SLASH;
export const TODO_LINK = `${SLASH}${TODO.toLowerCase()}`;
export const USER_LINK = `${SLASH}${USER.toLowerCase()}`;
export const TASK_DETAILS_LINK = `${SLASH}${TODO.toLowerCase()}${SLASH}${COLON}${ID.toLowerCase()}`;