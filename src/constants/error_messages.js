export const OBJECT_PROPERTY_DOES_NOT_EXIT = "Object property name doesn't exist";

export const USER_NOT_LOGGED_IN = "User is not logged in";

export const EMPTY_TASK_ID = "Please select a valid task id";
export const EMPTY_TASK_TITLE = "Please set a title for the task";