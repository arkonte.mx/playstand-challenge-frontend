export const ZERO = 0;
export const ONE = 1;
export const TWO = 2;

export const EMPTY_STRING = "";
export const SPACE = " ";
export const SLASH = "/";
export const COLON = ":";
export const UNDERSCORE = "_";
export const AT = "@";
export const ASTERISK = "*";

export const OK = "OK";