export const LOCAL_STORAGE_KEY = "paystand_challenge";

export const STORED_USER = "stored_user";

export const PERSISTED_LOGIN_LOG = "persisted_login_log";
export const PERSISTED_TASKS = "persisted_tasks";
