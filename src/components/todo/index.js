
import React from 'react';
import { connect } from 'react-redux';

import { Row, Col, Card } from 'antd';

import { IS_NOT_LOGGED_IN } from '../../constants/user';

import Tasks from '../tasks';
import Welcome from '../welcome';

const Todo = ({ is_logged_in = IS_NOT_LOGGED_IN }) => {

  return <Row justify="center">
    <Col span={24}>

      <Card bordered hoverable>
        {is_logged_in
          ? <Tasks />
          : <Welcome />
        }
      </Card>

    </Col>
  </Row>
};

const mapStateToProps = ({ user = {}, task = {} }) => ({
  is_logged_in: user.is_logged_in
});

export default connect(mapStateToProps, {})(Todo);