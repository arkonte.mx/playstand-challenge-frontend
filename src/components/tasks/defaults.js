import { EMPTY_STRING } from "../../constants/common";

export const create_task = {
  show_form: false,
  is_processing_task_creation: false
};

export const search_task = {
  title: EMPTY_STRING
};

export const filter_task = {
  show_completed: false
}