import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';

import { Row, Col, Button, Modal, Input, Form, List, Card } from 'antd';
import { PlusOutlined, DeleteOutlined, FieldTimeOutlined, CheckOutlined, SearchOutlined, ExpandAltOutlined } from '@ant-design/icons';

import { EMPTY_STRING, ZERO, TWO, SLASH } from '../../constants/common';

import { createTaskAction, deleteTaskAction, completeTaskAction } from '../../redux/taskDuck';

import { filterTasksBy } from './helpers';
import { create_task, search_task } from './defaults';
import { TODO_LINK } from '../../constants/links';

const Tasks = ({ is_processing = false, tasks = [], uid = EMPTY_STRING, createTaskAction, deleteTaskAction, completeTaskAction }) => {

  const { Item: FormItem } = Form;
  const { Item: ListItem } = List;
  const { Meta } = Card;

  const [ show_form, setShowForm ] = useState(create_task.show_form);
  const [ show_completed, setShowCompleted ] = useState(create_task.show_form);

  const [ title_search, setTitleSearch ] = useState(search_task.title);

  const [ form_reference ] = Form.useForm();

  let history = useHistory();

  const showCreateTaskForm = () =>
    setShowForm(!show_form);

  const cancelTaskCreation = () =>
    setShowForm(false);

  const createTask = (task = {}) => {
    setShowForm(false);
    form_reference.resetFields();

    createTaskAction(task);
    setTitleSearch(EMPTY_STRING);
  }

  const updateCompletedTask = (id = ZERO) => {
    completeTaskAction(id);
  }

  const deleteTask = (id = ZERO) => {
    deleteTaskAction(id);
  }

  const defineTitleSearch = ({ target = {} }) => {
    const search = (target.value || EMPTY_STRING)
      .trim()
      .toLowerCase();

    setTitleSearch(search);
  }

  const getTasksToShow = (tasks_to_filter = []) => {
    const tasks_to_show = (show_completed)
      ? tasks_to_filter
      : tasks_to_filter.filter(task => task.completed === false);

    if(title_search.length < TWO) {
      return tasks_to_show;
    }

    const by = { title: title_search };
    return filterTasksBy(tasks_to_show, by);
  }

  const openTaskDetails = (id = ZERO) => {
    history.push(`${TODO_LINK}${SLASH}${id}`, { id, uid });
  }

  const listCompletedTasks = () =>
    setShowCompleted(!show_completed);



  const getTaskActions = (id = ZERO, completed = false) => {
    const completed_icon = (completed)
      ? <div>
          <CheckOutlined style={{ color: "#228B22" }} />
          <span onClick={() => updateCompletedTask(id)} style={{ fontSize: "0.8em" }}>Completed</span>
        </div>
      : <div>
          <FieldTimeOutlined style={{ color: "#EEE8AA" }} />
          <span onClick={() => updateCompletedTask(id)} style={{ fontSize: "0.8em" }}>Still pending</span>
        </div>;

    const details_icon = <div>
      <ExpandAltOutlined icon={<ExpandAltOutlined />} style={{ color: "#4B0082" }} disabled={is_processing} loading={is_processing} />
      <span onClick={() => openTaskDetails(id)} style={{ fontSize: "0.8em" }}>Details</span>
    </div>;

    const delete_icon = <div>
      <DeleteOutlined style={{ color: "#DC143C" }} disabled={is_processing} loading={is_processing} />
      <span onClick={() => deleteTask(id)} style={{ fontSize: "0.8em" }}>Delete</span>
    </div>;

    return [ completed_icon, details_icon, delete_icon ];
  }

  return <Row  gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}  justify="center">

    <Col className="gutter-row" span={24}>
      <Row justify="space-between">
        <Col xs={23} sm={23} md={23} lg={15}>
          <Input placeholder="Search by title" onChange={defineTitleSearch} size="large" addonAfter={<SearchOutlined />} allowClear />
        </Col>
        <Col xs={23} sm={23} md={23} lg={7} >

          <Button type={(show_completed) ? "dashed" : "primary" } size="large" onClick={listCompletedTasks} block>
            { (show_completed) ? "Show pending only" : "Show all" }
          </Button>

        </Col>
      </Row>
    </Col>

    <Col className="gutter-row" span={24} style={{ overflowY: "scroll", height: "55vh", margin: "30px 0 30px 0" }}>
      <List
        loading={is_processing}
        dataSource={getTasksToShow(tasks)}
        renderItem={ ({ id = ZERO, title = EMPTY_STRING, description = EMPTY_STRING, completed = false }) =>
          <ListItem >
            <Col span={24}>
              <Card actions={getTaskActions(id, completed)}>
                  <Meta title={<strong>{title}</strong>} description={description} />
              </Card>
            </Col>
          </ListItem>
        } />
    </Col>

    <Col className="gutter-row" span={24}>
      <Button type="primary" icon={<PlusOutlined />} size="large" onClick={showCreateTaskForm} block>
        Add new task
      </Button>

      <Modal
        title="Create a task"
        visible={show_form}
        onCancel={cancelTaskCreation}
        footer={[]} >

          <Form
            name="create_task"
            form={form_reference}
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 18 }}
            onFinish={createTask}>

              <FormItem label="Title" name="title" labelAlign="left" rules={[{ required: true, message: "Please set a title" }]}>
                <Input />
              </FormItem>

              <FormItem label="Description" name="description" labelAlign="left">
                <Input />
              </FormItem>

              <FormItem style={{ justifyContent: 'center' }}>
                <Button type="primary" htmlType="submit" disabled={is_processing} loading={is_processing} block>
                    Create task
                </Button>
              </FormItem>
          </Form>
      </Modal>
    </Col>
  </Row>;
}

const mapStateToProps = ({ user = {}, task = {} }) => ({
  is_processing: task.is_processing,
  tasks: task.tasks,
  uid: user.uid
});

export default connect(mapStateToProps, { createTaskAction, deleteTaskAction, completeTaskAction })(Tasks);