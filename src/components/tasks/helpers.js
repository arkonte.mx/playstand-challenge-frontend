import { EMPTY_STRING, ZERO } from "../../constants/common";

export const filterTasksBy = (tasks = [], by = {}) => tasks
  .filter(task => Object.keys(by).every(key => {
    const value = by[key];

    if(typeof value === "string")
      return (task[key] || EMPTY_STRING).toLowerCase().includes(value.toLowerCase());

    if(typeof value === "number")
      return (task[key] || ZERO) === value;

    return true;
  }));