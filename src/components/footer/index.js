import React from 'react';

import { Row, Col } from 'antd';
import { MailOutlined, GitlabOutlined, LinkedinOutlined } from '@ant-design/icons';

const Footer = () => <Row justify="center">
  <Col xs={24} sm={12} md={8} lg={8} style={{ display: "flex", justifyContent: "space-evenly" }}>{"Jonathan Muñoz Lucas"}</Col>
  <Col xs={24} sm={12} md={8} lg={8} style={{ display: "flex", justifyContent: "space-evenly" }}>

    <a href="mailto:arkonte.mx@gmail.com">
      <MailOutlined style={{ fontSize: "1.6em" }} />
    </a>
    <a href="https://gitlab.com/arkonte.mx" target="_blank" rel="noreferrer">
      <GitlabOutlined style={{ fontSize: "1.6em" }} />
    </a>
    <a href="https://www.linkedin.com/in/arkontemx/" target="_blank" rel="noreferrer">
      <LinkedinOutlined style={{ fontSize: "1.6em" }} />
    </a>

  </Col>
</Row>;

export default Footer;