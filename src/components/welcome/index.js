
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import { Row, Col, Card, Timeline, Statistic, Divider } from 'antd';
import { GoogleCircleFilled, GitlabOutlined } from '@ant-design/icons';

import { retrieveReviewHistory } from '../../firebase';
import { EMPTY_STRING, ZERO, ASTERISK, AT } from '../../constants/common';

const SECOND_AS_MILLISECONDS = 1000;
const CHECK_AGAIN_SECONDS = 30;
const EMAIL_SHOW_CHARACTER_COUNT = 5;

const getHiddenEmail = (email = EMPTY_STRING) => {
  const visible_characters = email.substr(ZERO, EMAIL_SHOW_CHARACTER_COUNT);
  const end_of_visible_characters = (email.length - EMAIL_SHOW_CHARACTER_COUNT);
  const masking_characters = ASTERISK.repeat(end_of_visible_characters);
  const at_index = email.indexOf(AT);
  const masked_mail = (visible_characters + masking_characters);
  return masked_mail.substring(ZERO, at_index) + AT + masked_mail.substring(at_index+1);
}

const getCountdownEpoch = () =>
  Date.now() + CHECK_AGAIN_SECONDS * SECOND_AS_MILLISECONDS;

const Welcome = () => {

  const { Item } = Timeline;
  const { Countdown } = Statistic;

  const [ countdown, setCountdown ] = useState(getCountdownEpoch());
  const [ review_history, setReviewHistory ] = useState([]);

  useEffect(() => {
    const fetchHistory = async () => {
      const response = await retrieveReviewHistory();
      const history = response.docs.map(document => document.data());

      if(history.length > review_history.length) {
        setReviewHistory(history);
      }
    }

    fetchHistory();
  });

  const resetCountdown = () =>
    setCountdown(getCountdownEpoch());

  return <Row>
    <Col span={24} style={{ margin: "30px 0 30px 0" }}>
      <Countdown title="Countdown to check if there are new reviews" value={countdown} onFinish={resetCountdown} />

      <Card title={"PayStand challenge todo app"} bordered>
        <Row>

          <Col span={24}>
            <p>
              In order to use the app you must login with a google account.
              To do so please click on the "User" menu to the left of this screen,
              there you'll find a button, click it to login.
            </p>
            <p><strong>IMPORTANT!, YOU MUST ALLOW POP-UPS FOR THE LOGIN TO WORK.</strong></p>
            <p><GitlabOutlined /> <a href="https://gitlab.com/arkonte.mx/playstand-challenge-frontend" target="_blank" rel="noreferrer">Frontend repo</a></p>
            <p><GitlabOutlined /> <a href="https://gitlab.com/arkonte.mx/playstand-challenge-backend" target="_blank" rel="noreferrer">Backend repo</a></p>
          </Col>

        </Row>
        <Divider />
        <Row>

          <Col span={24}>
            <strong>{`This app has been reviewed ${review_history.length} times`}</strong>
            <Timeline mode="alternate" style={{ height: "15vh", overflowX: "hidden", overflowY: "scroll", padding: "20px" }}>
              {review_history.map(({ email = EMPTY_STRING, timestamp = (new Date()).toISOString() }, index = ZERO) => <Item
                key={index}
                color="red"
                dot={<GoogleCircleFilled />}
                label={new Date(timestamp).toLocaleDateString()} >
                  {`By ${getHiddenEmail(email)}`}
                </Item>
              )}
            </Timeline>
          </Col>

        </Row>
      </Card>

    </Col>
  </Row>
};

const mapStateToProps = () => ({
});

export default connect(mapStateToProps, {})(Welcome);