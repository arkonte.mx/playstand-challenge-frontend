
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import axios from 'axios';

import { Row, Col, Card, Form, Input, Switch, Tag, Button } from 'antd';
import { CheckCircleOutlined, ClockCircleOutlined, CalendarFilled } from '@ant-design/icons';

import { updateTaskAction } from '../../redux/taskDuck';
import { areObjectsShallowEqual } from '../../utils/comparison';

import { EMPTY_STRING, ZERO } from '../../constants/common';
import { TODO_LINK } from '../../constants/links';

const API_URL = process.env.REACT_APP_API_URL;

const Task = ({ updateTaskAction }) => {

  const [ form ] = Form.useForm();
  const { Item } = Form;

  const [ task, setTask ] = useState({});
  const [ is_processing, setProcessing ] = useState(false);

  let history = useHistory();
  const { state: historyState = {} } = history.location;
  const { id = ZERO, uid = EMPTY_STRING } = historyState;

  useEffect(() => {
    const fetchTaskData = async () => {
      const { data = {} } = await axios.get(`${API_URL}/:id`, { params: { id, uid } });

      if(!areObjectsShallowEqual(task, data)) {
        setTask(data);
      }
    }

    form.setFieldsValue({
      title: task.title,
      description: task.description,
      completed: task.completed
    });

    fetchTaskData();
  });

  const updateTask = async (update = {}) => {
    setProcessing(true);

    await updateTaskAction({ ...update, id });
    history.push(`${TODO_LINK}`);
  }

  return <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify="center">
    <Col className="gutter-row" span={24}>

      <Card>
        <Form name="task"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          form={form}
          onFinish={updateTask}>

            <Item rules={[ { required: true, message: "Title can't be empty" } ]}
              label="Title" name="title">
                <Input />
            </Item>

            <Item label="Description" name="description">
              <Input />
            </Item>

            <Item label="Completed" name="completed" valuePropName="checked">
              <Switch checkedChildren={<CheckCircleOutlined />} unCheckedChildren={<ClockCircleOutlined />} />
            </Item>

            <Item label="Last modified">
              <Tag icon={<CalendarFilled />} color="default">
                {new Date(task.updatedAt).toLocaleString(undefined, {year: 'numeric', month: '2-digit', day: '2-digit'})}
              </Tag>
            </Item>

            <Item style={{ justifyContent: 'center' }}>
              <Button type="primary" loading={is_processing} htmlType="submit" ghost block disabled={is_processing}>
                  Update task
              </Button>
            </Item>

        </Form>
      </Card>

    </Col>
  </Row>;
};

const mapStateToProps = () => ({
});

export default connect(mapStateToProps, { updateTaskAction })(Task);