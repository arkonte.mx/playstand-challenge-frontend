import React from 'react';
import { connect } from 'react-redux';

import { Button } from 'antd';
import { GoogleOutlined } from '@ant-design/icons';

import { loginWithGoogleAction, logoutFromGoogleAction } from '../../redux/userDuck';

import { IS_NOT_LOGGED_IN } from '../../constants/user';
import { LOGIN_WITH_GOOGLE, LOGOUT } from './constants';

const UserLogin = ({ is_logged_in = IS_NOT_LOGGED_IN, loginWithGoogleAction, logoutFromGoogleAction }) => {

    const login = () =>
      loginWithGoogleAction();

    const logout = () =>
      logoutFromGoogleAction();

    return (is_logged_in)
      ? <Button onClick={logout} icon={<GoogleOutlined />} shape="round" danger block ghost>
          { LOGOUT }
        </Button>
      : <Button onClick={login} icon={<GoogleOutlined />} type="primary" shape="round" danger block>
          { LOGIN_WITH_GOOGLE }
        </Button>;
}

const mapStateToProps = ({ user = {} }) => ({
  is_logged_in: user.is_logged_in
});

export default connect(mapStateToProps, { loginWithGoogleAction, logoutFromGoogleAction })(UserLogin);