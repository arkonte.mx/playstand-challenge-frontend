
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'

import { Layout, Menu, Row, Col } from 'antd';
import { UserSwitchOutlined, BarsOutlined, IdcardFilled } from '@ant-design/icons';

import UserLogin from '../user-login';
import LayoutFooter from '../footer';

import { UNDERSCORE } from '../../constants/common';
import { TODO_LINK, USER_LINK } from '../../constants/links';
import { LAYOUT, LOGO, INLINE, USER, DARK, TODO, LOGIN, DETAILS, SIDER_WIDTH } from './constants';
import { WHITE_COLOR_HEX } from '../../constants/color';

import * as defaults from './defaults';
import './style.css';
import { MD } from '../../constants/screen_size';

const getWindowDimensions =() => {
  const { innerWidth, innerHeight } = window;
  return { innerWidth, innerHeight };
}

const AppLayout = ({ children, is_logged_in = true }) => {

  const { Header, Footer, Sider, Content } = Layout;
  const { Item, SubMenu } = Menu;

  const [ is_collapsed, setIsCollapsed ] = useState(defaults.sider.is_collapsed);
  const [ window_dimensions, setWindowDimensions ] = useState(getWindowDimensions());

  const { innerWidth } = window_dimensions;
  const should_hide_contend = (innerWidth < MD && !is_collapsed);
  const slider_width = (should_hide_contend) ? "100%" : SIDER_WIDTH;

  useEffect(() => {
    const handleWindowResize = () =>
      setWindowDimensions(getWindowDimensions());

    window.addEventListener('resize', handleWindowResize);

    return () =>
      window.removeEventListener('resize', handleWindowResize);
  });

  const collapseSider = (collapse) =>
    setIsCollapsed(collapse);

  const selectSubMenu = () => {
    if(!should_hide_contend && is_collapsed) {
      collapseSider(false);
    }
  }

  const style= {
    header: {
      background: `#${WHITE_COLOR_HEX}`
    },
    content: {
      margin: "24px 16px",
      padding: "24px",
      background: `#${WHITE_COLOR_HEX}`
    }
  };

  return <Layout className={LAYOUT}>
    <Sider collapsed={is_collapsed} onCollapse={collapseSider} width={slider_width} collapsible>

      <div className={LOGO} />
      <Menu theme={DARK} defaultSelectedKeys={[TODO]} mode={INLINE} onSelect={() => collapseSider(true)}>

        <Item key={TODO} icon={<BarsOutlined />}>
          <NavLink to={TODO_LINK}>
            {TODO}
          </NavLink>
        </Item>

        <SubMenu key={`${USER}${UNDERSCORE}${LOGIN}`} icon={<UserSwitchOutlined />} title={USER} onTitleClick={selectSubMenu}>
          {is_logged_in
            ? <Item key={DETAILS} icon={<IdcardFilled />}>
                <NavLink to={USER_LINK}>
                  {DETAILS}
                </NavLink>
              </Item>
            : <Item key={LOGIN} style={{ paddingLeft: "16px" }} disabled>
                <UserLogin />
              </Item>
          }
        </SubMenu>

      </Menu>
    </Sider>

    {(!should_hide_contend) &&
      <Layout>
        <Header style={style.header}>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify="center">
            <Col className="gutter-row" xs={24} sm={20} md={16} lg={14}>

            </Col>
          </Row>
        </Header>


        <Content style={style.content}>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify="center">
            <Col className="gutter-row" xs={24} sm={20} md={16} lg={14}>
              { children }
            </Col>
          </Row>
        </Content>

        <Footer>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify="center">
            <Col className="gutter-row" xs={24} sm={20} md={16} lg={14}>
              <LayoutFooter />
            </Col>
          </Row>
        </Footer>

      </Layout>
    }

  </Layout>;
}


const mapStateToProps = ({ user = {} }) => ({
  is_logged_in: user.is_logged_in
});

export default connect(mapStateToProps, {})(AppLayout);