export const LAYOUT = "layout";
export const LOGO = "logo";
export const INLINE = "inline";
export const DARK = "dark";

export { TODO, USER, LOGIN } from '../../constants/links';

export const DETAILS = "Details";

export const SIDER_WIDTH = "220px";
