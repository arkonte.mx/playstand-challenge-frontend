
import React from 'react';
import { connect } from 'react-redux';

import { Row, Col, Card } from 'antd';

import { EMPTY_STRING } from '../../constants/common';
import UserLogin from '../user-login';

const UserDetails = ({ displayName = EMPTY_STRING, email = EMPTY_STRING, photoURL = EMPTY_STRING }) => {
  const { Meta } = Card;

  return <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify="center">
    <Col className="gutter-row" xs={24} sm={20} md={16} lg={12}>

      <Card cover={ <img alt="Account avatar" src={photoURL} /> } title={displayName}>
        <Meta description={email} style={{ margin: "10px", textAlign: "center" }}/>
        <UserLogin />
      </Card>

    </Col>
  </Row>;
};

const mapStateToProps = ({ user = {} }) => ({
  is_logged_in: user.is_logged_in,
  displayName: user.displayName,
  email: user.email,
  photoURL: user.photoURL
});

export default connect(mapStateToProps, {})(UserDetails);