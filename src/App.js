import Routes from './Routes';
import AppLayout from './components/app-layout';

import './App.css';

const App = () => <AppLayout>
  <Routes />
</AppLayout>;

export default App;