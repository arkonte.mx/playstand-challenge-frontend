import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { EMPTY_STRING } from './constants/common';
import { PERSISTED_LOGIN_LOG } from './constants/storage';

const firebaseConfig = {
  apiKey: "AIzaSyBNV9vECArsgvWKTRKGvGK6walz5V8gnZk",
  authDomain: "curso-react-redux-graphq-59e36.firebaseapp.com",
  projectId: "curso-react-redux-graphq-59e36",
  storageBucket: "curso-react-redux-graphq-59e36.appspot.com",
  messagingSenderId: "418833844742",
  appId: "1:418833844742:web:fc814bc4759fca9c081187"
};

firebase.initializeApp(firebaseConfig);
const persistance_login_log = firebase.firestore().collection(PERSISTED_LOGIN_LOG);

export const loginWithGoogle = async () => {
  const provider = new firebase.auth.GoogleAuthProvider();
  return await firebase.auth().signInWithPopup(provider);
}

export const logoutFromGoogle = async () =>
  firebase.auth().signOut();

export const logLogin = async ({ uid = EMPTY_STRING, email = EMPTY_STRING }) => {
  const timestamp = (new Date()).toISOString();

  persistance_login_log.doc(uid)
    .set({ email, timestamp });
}

export const retrieveReviewHistory = async () =>
  await persistance_login_log.get();
