export const areObjectsShallowEqual = (object_one = {}, object_two = {}) =>
  Object.keys(object_one).length === Object.keys(object_two).length &&
  Object.keys(object_one).every(key => object_one[key] === object_two[key]);