import { OBJECT_PROPERTY_DOES_NOT_EXIT } from "../constants/error_messages";
import { LOCAL_STORAGE_KEY } from "../constants/storage";

const retrieveFromLocalStorage = () =>
    JSON.parse(
        localStorage.getItem(LOCAL_STORAGE_KEY) || '{}');

const setIntoLocalStorage = (data) =>
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(data));

export const addToLocalStorage = async (key, value) => {
    const data = { ...retrieveFromLocalStorage(), [key]: value };
    setIntoLocalStorage(data);
}

export const removeFromLocalStorage = async (key) => {
    let data = retrieveFromLocalStorage();

    if(data[key] === undefined) {
        return new Error(OBJECT_PROPERTY_DOES_NOT_EXIT);
    }

    delete data[key];
    setIntoLocalStorage(data);
}

export const getFromLocalStorage = (key) =>
  retrieveFromLocalStorage()[key];

export const clearLocalStorage =  async () =>
  localStorage.removeItem(LOCAL_STORAGE_KEY);