import { STORED_USER } from "../constants/storage";
import { getFromLocalStorage } from "./local_storage";

export const isLoggedIn = (redux_user = {}) => {
  const local_storage_user = getFromLocalStorage(STORED_USER) || {};
  return (redux_user.uid && local_storage_user.uid) && (redux_user.uid === local_storage_user.uid);
}