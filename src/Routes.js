import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Todo from './components/todo';
import UserDetails from './components/user-details';
import Task from './components/task';

import { ROOT_LINK, TASK_DETAILS_LINK, TODO_LINK, USER_LINK } from './constants/links';
import { isLoggedIn } from './utils/session';

const PrivateRoute = ({ path = '', component, ...props }) => (isLoggedIn(props.user))
  ? <Route path={ path } component={ component } { ...props } />
  : <Redirect to={TODO_LINK} { ...props } />;

const Routes = ({ user = {} }) => <Switch>
    <Route exact path={ROOT_LINK} component={Todo} />
    <Route exact path={TODO_LINK} component={Todo} />
    <PrivateRoute path={TASK_DETAILS_LINK} component={Task} user={user}/>
    <PrivateRoute path={USER_LINK} component={UserDetails} user={user}/>
</Switch>;

const mapStateToProps = ({ user = {} }) => ({
  user
});

export default connect(mapStateToProps)(Routes);